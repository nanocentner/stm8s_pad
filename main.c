#include "stm8s.h"

#define A_PINS (GPIO_PIN_1 | GPIO_PIN_2)
#define B_PINS (GPIO_PIN_4 | GPIO_PIN_5)
#define C_PINS (GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7)
#define D_PINS (GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6)

void Delay(uint16_t nCount)
{
  /* Decrement nCount value */
  while (nCount != 0)
  {
    nCount--;
  }
}

int main( void )
{
  GPIO_Init(GPIOA, (GPIO_Pin_TypeDef)A_PINS, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(GPIOB, (GPIO_Pin_TypeDef)B_PINS, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(GPIOC, (GPIO_Pin_TypeDef)C_PINS, GPIO_MODE_OUT_PP_LOW_FAST);
  GPIO_Init(GPIOD, (GPIO_Pin_TypeDef)D_PINS, GPIO_MODE_OUT_PP_LOW_FAST);
  
  while (1)
  {
    /* Toggles LEDs */
    GPIO_WriteReverse(GPIOA, (GPIO_Pin_TypeDef)A_PINS);
    GPIO_WriteReverse(GPIOB, (GPIO_Pin_TypeDef)B_PINS);
    GPIO_WriteReverse(GPIOC, (GPIO_Pin_TypeDef)C_PINS);
    GPIO_WriteReverse(GPIOD, (GPIO_Pin_TypeDef)D_PINS);
  }
  
  return 0;
}

#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif